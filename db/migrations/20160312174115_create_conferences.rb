Hanami::Model.migration do
  change do
    create_table :conferences do
      primary_key :id
      foreign_key :teacher_id, :teachers, null: false
      foreign_key :slot_id, :slots, null: false
      foreign_key :parent_id, :parents
      foreign_key :month_id, :months, null: false

      column :day, Integer,  null: false
      column :status, String, null: false
    end
  end
end
