Hanami::Model.migration do
  change do
    alter_table :conferences do
      add_column :year, Integer
      add_column :date, String
      add_column :uid, String, unique: true
    end
  end
end
