Hanami::Model.migration do
  change do
    alter_table :months do
      add_column :seq_no, Integer
    end
  end
end
