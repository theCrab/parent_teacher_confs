Hanami::Model.migration do
  change do
    create_table :settings do
      primary_key :id

      column :key,    String
      column :value,  String
    end
  end
end
