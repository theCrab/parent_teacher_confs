Hanami::Model.migration do
  change do
    alter_table :conferences do
      add_column :parent_comment, String
    end
  end
end
