Hanami::Model.migration do
  change do
    create_table :slots do
      primary_key :id

      column :period,  String,  null: false
    end
  end
end
