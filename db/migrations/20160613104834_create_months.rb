Hanami::Model.migration do
  change do
    create_table :months do
      primary_key :id

      column :name,   String
    end
  end
end
