Hanami::Model.migration do
  change do
    create_table :parents do
      primary_key :id

      column :name,  String,  null: false
      column :surname, String,  null: false
      column :email, String, null: false, unique: true
      column :password_hash, String, null: false
      column :password_salt, String, null: false
      column :password_reset_token, String
      column :password_reset_sent_at, DateTime
    end
  end
end