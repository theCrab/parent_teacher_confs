require 'hanami/model'
require 'hanami/mailer'
Dir["#{ __dir__ }/ptconference/**/*.rb"].each { |file| require_relative file }

Hanami::Model.configure do
  ##
  # Database adapter
  #
  # Available options:
  #
  #  * Memory adapter
  #    adapter type: :memory, uri: 'memory://localhost/ptconference_development'
  #
  #  * SQL adapter
  #    adapter type: :sql, uri: 'sqlite://db/ptconference_development.sqlite3'
  #    adapter type: :sql, uri: 'postgres://localhost/ptconference_development'
  #    adapter type: :sql, uri: 'mysql://localhost/ptconference_development'
  #
  adapter type: :sql, uri: ENV['PTCONFERENCE_DATABASE_URL']

  ##
  # Database mapping
  #
  # Intended for specifying application wide mappings.
  #
  # You can specify mapping file to load with:
  #
  mapping "#{__dir__}/config/mapping"
  #
  # Alternatively, you can use a block syntax like the following:
  #
  #mapping do
  #   collection :students do
  #     entity     Student
  #     repository StudentRepository
  #  
  #     attribute :id,   Integer
  #     attribute :name, String
  #     attribute :surname, String
  #   end
  #end
end.load!

Hanami::Mailer.configure do
  root "#{ __dir__ }/ptconference/mailers"

  # See http://hanamirb.org/guides/mailers/delivery
  delivery do
    development :smtp,
      address:              "smtp.gmail.com",
      port:                 587,
      #domain:               "bookshelf.org",
      user_name:            'sebastjan.hribar@gmail.com',
      password:             'NeMorem1234Spet56',
      authentication:       "plain",
      enable_starttls_auto: true
    #development :test
    test        :test
    # production :stmp, address: ENV['SMTP_PORT'], port: 1025
  end
end.load!
