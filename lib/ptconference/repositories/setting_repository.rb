class SettingRepository
  include Hanami::Repository
  
  
  def self.get_booking_lock()
    query do
      where(key: "booking_lock")
    end.first
  end
  
end