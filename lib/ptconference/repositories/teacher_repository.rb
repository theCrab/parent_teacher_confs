class TeacherRepository
  include Hanami::Repository
  
  def self.user_with_email(email)
    query do
      where(email: email)
    end.limit(1).first unless email.blank?
  end
  
  def self.user_by_token(token)
    query do
      where(password_reset_token: token)
    end.limit(1).first
  end
  
end
