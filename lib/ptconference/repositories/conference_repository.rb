class ConferenceRepository
  include Hanami::Repository

  # Gets all conferences for a given teacher.
  # Regardless of the booking state (parent id).
  def self.teacher_conferences(teacher_id)
    query do
      where(teacher_id: teacher_id, status: "Aktiven").
          asc(:month_id).
            asc(:slot_id)
    end
  end

  # Gets all active conferences for a given teacher and month.
  # Regardless of the booking state (parent id).

  # If teacher is not selected and the value defaults to "Izberi učitelja",
  # conferences are queried only on the basis of the month.
  def self.teacher_conferences_for_deactivation(month_id, teacher_id)
    query do
      if teacher_id == "Izberi učitelja"
        where(month_id: month_id, status: "Aktiven").
          asc(:month_id).
            asc(:slot_id)
      else
        where(month_id: month_id, teacher_id: teacher_id, status: "Aktiven").
          asc(:month_id).
            asc(:slot_id)
      end
    end
  end

  # Gets all deactivated conferences for a given teacher and month.
  # Regardless of the booking state (parent id).
  def self.teacher_conferences_for_reactivation(month_id, teacher_id)
    query do

      if teacher_id == "Izberi učitelja"
        where(month_id: month_id, status: "Neaktiven").
          asc(:month_id).
            asc(:slot_id)
      else
        where(month_id: month_id, teacher_id: teacher_id, status: "Neaktiven").
          asc(:month_id).
            asc(:slot_id)
      end
    end
  end

  # Gets all conferences for which the parent has been booked.
  def self.parent_conferences(parent_id)
    query do
      where(parent_id: parent_id, status: "Aktiven").
          asc(:month_id).
            asc(:slot_id)
    end
  end

  # Gets all conferences for which the parent hasn't been booked.
  # Regardless of other parameters.
  def self.available_conferences
    query do
      where(parent_id: 0, status: "Aktiven").
          asc(:month_id).
            asc(:slot_id)
    end
  end

  # Gets all conferences for the current month and for
  # which the parent hasn't been booked.
  # Regardless of other parameters.
  def self.unbooked_conferences(month_id, teacher)
    query do
      where(teacher_id: teacher, month_id: month_id, parent_id: 0, status: "Aktiven").
          asc(:month_id).
            asc(:slot_id)
    end
  end

  # Gets the first conference that can be booked for the given
  # month, teacher and period.
  def self.conference_for_booking(month_id, teacher_id, slot_id)
    query do
      where(month_id: month_id, teacher_id: teacher_id, slot_id: slot_id, status: "Aktiven")
    end.first
  end

  # Gets the relevant conference day for a given month.
  def self.relevant_day(month_id)
    query do
      where(month_id: month_id, status: "Aktiven")
    end.first
  end

  # Gets all relevant conferences available for booking for a given
  # month and teacher.
  def self.conferences_for_booking(month_id, teacher_id)
    query do
      where(month_id: month_id, teacher_id: teacher_id, parent_id: 0, status: "Aktiven").
          asc(:month_id).
            asc(:slot_id)
    end
  end

  # Gets a conference for a certain month, teacher and parend. To enable
  # the check for an existing booking for the above combination of params.
  def self.check_if_teacher_month_booking_exists(month_id, teacher_id, parent_id)
    query do
      where(month_id: month_id, teacher_id: teacher_id, parent_id: parent_id, status: "Aktiven")
    end.first
  end

  # Gets the slot for a given month, period and parent. To avoid double
  # booking of a parent to several teachers in the same period and month.
  def self.booked_slots_for_month(month_id, slot_id, parent_id)
    query do
      where(month_id: month_id, slot_id: slot_id, parent_id: parent_id, status: "Aktiven")
    end.first
  end

  # Get all conferences where the parent teacher conference day is to be changed.
  # This doesn't depend on the conference status. It does take into account if
  # only month is selected or the teacher is selected as well.
  def self.conferences_for_day_change(month_id, teacher_id)
    query do
      if teacher_id == "Izberi učitelja"
        where(month_id: month_id).
          asc(:month_id).
            asc(:slot_id)
      else
        where(month_id: month_id, teacher_id: teacher_id).
          asc(:month_id).
            asc(:slot_id)
      end
    end
  end

end
