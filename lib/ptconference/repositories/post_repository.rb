class PostRepository
  include Hanami::Repository

  def self.recent_posts(limit: 3)
    query do
      desc(:published_at).limit(limit)
    end
  end

end
