class Post
  include Hanami::Entity
  
  attributes :title, :body, :published_at
end
