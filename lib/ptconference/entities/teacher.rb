class Teacher
  include Hanami::Entity
  attributes :name, :surname, :email, :password, :password_hash,
  :password_salt, :password_reset_token, :password_reset_sent_at, :role
end
