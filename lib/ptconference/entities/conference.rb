class Conference
  include Hanami::Entity
  attributes :month_id, :day, :teacher_id, :status, :slot_id, :parent_id, :parent_comment, :year, :date, :uid
end
