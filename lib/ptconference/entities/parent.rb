class Parent
  include Hanami::Entity
  
  attributes :name, :surname, :email, :password, :password_hash, :password_salt,
  :password_confirmation, :password_reset_token, :password_reset_sent_at

end
