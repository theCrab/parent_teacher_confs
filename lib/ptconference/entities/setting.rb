class Setting
  include Hanami::Entity
  
  attributes :key, :value
end