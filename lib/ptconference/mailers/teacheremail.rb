class Mailers::Teacheremail
  include Hanami::Mailer

  from    :sender
  to      :recipient
  subject :message_title
  

  private
  
  def sender
    teacher_email
  end
  
  def recipient
    parent_email
  end
  
  def message
    body
  end
  
  def message_title
    title
  end
end
