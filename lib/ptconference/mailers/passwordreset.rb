class Mailers::Passwordreset
  include Hanami::Mailer

  from    'sebastjan.hribar@gmail.com'
  to      :recipient
  subject :mail_title
  

  private
  
  def emailbody
    mail_body
  end
  
  def recipient
    user_email
  end

end
