class Mailers::Daychangenotice
  include Hanami::Mailer

  from    'sebastjan.hribar@gmail.com'
  to      :parents
  subject :post_title
  

  private
  
  def postbody
    post_body
  end
  
  def parent_list
    parents
  end
end
