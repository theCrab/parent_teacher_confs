collection :parents do
  entity     Parent
  repository ParentRepository

  attribute :id,                      Integer
  attribute :name,                    String
  attribute :surname,                 String
  attribute :email,                   String
  attribute :password_hash,           String
  attribute :password_salt,           String
  attribute :password_reset_token,    String
  attribute :password_reset_sent_at,  DateTime
end

collection :teachers do
  entity     Teacher
  repository TeacherRepository

  attribute :id,                      Integer
  attribute :name,                    String
  attribute :surname,                 String
  attribute :email,                   String
  attribute :role,                    String
  attribute :password_hash,           String
  attribute :password_salt,           String
  attribute :password_reset_token,    String
  attribute :password_reset_sent_at,  DateTime
end

collection :conferences do
  entity     Conference
  repository ConferenceRepository

  attribute   :id,              Integer
  attribute   :month_id,        Integer
  attribute   :day,             Integer
  attribute   :status,          String
  attribute   :teacher_id,      Integer
  attribute   :slot_id,         Integer
  attribute   :parent_id,       Integer
  attribute   :parent_comment,  String
  attribute   :year,            Integer
  attribute   :date,            String
  attribute   :uid,             String
end

collection :slots do
  entity     Slot
  repository SlotRepository

  attribute :id,          Integer
  attribute :period,      String
end

collection :posts do
  entity      Post
  repository  PostRepository

  attribute   :id,              Integer
  attribute   :title,           String
  attribute   :body,            String
  attribute   :published_at,    DateTime
end

collection :settings do
  entity     Setting
  repository SettingRepository

  attribute :id,          Integer
  attribute :key,         String
  attribute :value,       String
end

collection :months do
  entity      Month
  repository  MonthRepository

  attribute   :id,              Integer
  attribute   :name,            String
  attribute   :seq_no,          Integer
end
