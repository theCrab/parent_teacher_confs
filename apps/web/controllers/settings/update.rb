module Web::Controllers::Settings
  class Update
    include Web::Action

    before :check_for_admin_role

    params do
      param :id, presence: true

      param :setting do
        param :value, presence: true
      end
    end

    def call(params)
      if params.valid?
        @setting = SettingRepository.get_booking_lock
        @setting.update(params[:setting])
        @setting = SettingRepository.update(@setting)
        flash[:success_notice] = "Status blokade prijav govorilnih ur je bil uspešno spremenjen na #{@setting.value.capitalize}."
        redirect_to "/teacher_home"
      else
        flash[:failed_notice] = "Prišlo je do napake pri vnosu podatkov. Preverite podatke in
        poskusite ponovno."
        redirect_to "/settings/#{@setting.id}/edit"
      end

    end
  end
end
