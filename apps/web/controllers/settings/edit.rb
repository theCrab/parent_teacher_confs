module Web::Controllers::Settings
  class Edit
    include Web::Action

    before :check_for_admin_role

    expose :booking_lock_values, :setting

    def call(params)
      @setting = SettingRepository.get_booking_lock
      @booking_lock_values = booking_lock_listbox_values
    end
  end
end
