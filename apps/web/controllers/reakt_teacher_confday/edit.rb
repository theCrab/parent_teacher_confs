module Web::Controllers::ReaktTeacherConfday
  class Edit
    include Web::Action

    before :check_for_admin_role

    expose :months, :t_values

    def call(params)
      
      # A hash of teacher IDs and full names is created for the listbox
      # in the template.
      @t_values = teacher_listbox_values

      @months = month_listbox_values
    end
  end
end
