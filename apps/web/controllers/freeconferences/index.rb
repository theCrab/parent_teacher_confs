module Web::Controllers::Freeconferences
  class Index
    include Web::Action

    before :check_for_signed_in_user

    # This controller provides a list of conferences (combination
    # of teacher, month and period) which are still available
    # for booking (parent id = 0).

    # The list is then fed to the listbox in the template via JS.

    expose :periods

    def call(params)
      self.format = :json

      @conferences_for_booking = ConferenceRepository.conferences_for_booking(params[:month_id], params[:teacher_id])

      @periods = {}
      @conferences_for_booking.each {|conf|
        @periods[conf.slot_id] = SlotRepository.find(conf.slot_id).period
        }

    end
  end
end
