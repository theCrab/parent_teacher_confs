module Web::Controllers::Passwordreset
  class Update
    include Web::Action

    params do
      param :passwordreset do
        param :email, presence: true
        param :user_class, presence: true
      end
    end

    def call(params)
      if params.valid?
        # Sets variables from param values
        email = params[:passwordreset][:email]
        user_class = params[:passwordreset][:user_class]
        
        # Find the user in respective repository
        if user_class == "Starš"
          repository = ParentRepository
        else
          repository = TeacherRepository
        end
        
        user = repository.user_with_email(email)
        
        
        # Generate the token and update user
        token = SecureRandom.urlsafe_base64
        password_reset_sent_time = Time.now
        user.update(password_reset_token: token, password_reset_sent_at: password_reset_sent_time)
        user = repository.update(user)
        title = "Ponastavitev gesla"
        body = "http://localhost:2300/passwordupdate/#{token}"
        
        # Send the reset email
        Mailers::Passwordreset.deliver(mail_title: title, mail_body: body, user_email: email)
        
        
        flash[:success_notice] = "Povezava za ponastavitev gesla je bila poslana."
        redirect_to '/'

        
      else
        flash[:failed_notice] = "Prišlo je do napake pri vnosu podatkov. Preverite podatke in
        poskusite ponovno."
        redirect_to 'passwordreset/edit'
      end
    end
  end
end