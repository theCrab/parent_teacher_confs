module Web::Controllers::Temail
  class New
    include Web::Action

    before :check_for_teacher_class

    expose :parent_email_values

    def call(params)
      
      # A hash of parent IDs and emails is created for the listbox
      # in the template.
      @parent_email_values = parent_email_listbox_values
      
    end
  end
end
