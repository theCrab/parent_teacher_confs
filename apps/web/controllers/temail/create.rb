module Web::Controllers::Temail
  class Create
    include Web::Action

    before :check_for_teacher_class

    params do
      param :temail do
        param :title, presence: true, size: 3..20
        param :body, presence: true
        param :parent_email, presence: true, format: /\A(.*)@(\w*\d*)\.(\w*)\z/
      end
    end

    def call(params)
      if params.valid?
        teacher_email = session[:current_user].email
        parent_email = params[:temail][:parent_email]
        title = params[:temail][:title]
        body = params[:temail][:body]
        
        
        # Send email to the selected parent
        Mailers::Teacheremail.deliver(title: title, body: body, parent_email: parent_email, teacher_email: teacher_email)
        flash[:success_notice] = "E-pošta je bila uspešno poslana."
        redirect_to '/teacher_home'
      else
        flash[:failed_notice] = "Prišlo je do napake pri vnosu podatkov. Preverite podatke in
        poskusite ponovno."
        redirect_to '/temail/new'
      end
    end
  end
end
