module Web::Controllers::Getday
  class Index
    include Web::Action

    before :check_for_signed_in_user

    # This controller provides a day in a month when the conferences
    # are held.

    # The day is then fed to the template via JS.

    expose :day

    def call(params)
      @day = ConferenceRepository.relevant_day(params[:month_id]).day
    end
  end
end
