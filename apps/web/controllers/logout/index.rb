module Web::Controllers::Logout
  class Index
    include Web::Action

    # Logs the current user out by setting its session value to nil.
    # Users authorizations are then revoked by the Authorization module.

    def call(_)
      session[:current_user] = nil
      redirect_to '/'
    end
  end
end
