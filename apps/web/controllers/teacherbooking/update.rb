module Web::Controllers::Teacherbooking
  class Update
    include Web::Action

    before :check_for_teacher_class

    params do
      param :teacherbooking do
        param :month_id, presence: true
        param :parent_email, presence: true
        param :slot_id, presence: true
      end
    end

    def call(params)
      if params.valid?
        # Sets variables from param values
        month_id = params[:teacherbooking][:month_id]
        slot_id = params[:teacherbooking][:slot_id]
        teacher_id = session[:current_user].id

        period = SlotRepository.find(slot_id).period
        parent_email = params[:teacherbooking][:parent_email]
        parent_id = ParentRepository.user_with_email(parent_email).id

        # Check for booked conferences in a month and conferences with a teacher in a month.
        booked_slots = ConferenceRepository.booked_slots_for_month(month_id, slot_id, parent_id)
        existing_booking = ConferenceRepository.check_if_teacher_month_booking_exists(month_id, teacher_id, parent_id)

        # Control flow based on above checks
        if existing_booking
          flash[:failed_notice] = "Za mesec #{month_id} je starš že prijavljen k vam na govorilne ure."
          redirect_to 'teacherbooking/edit'

        elsif booked_slots
          flash[:failed_notice] = "Za mesec #{month_id} je starš že prijavljen za termin #{period}."
          redirect_to 'teacherbooking/edit'

        else
          @conference = ConferenceRepository.conference_for_booking(month_id, teacher_id, slot_id)

          if @conference.parent_id = 0
            @conference.update(parent_id: parent_id)
            @conference = ConferenceRepository.update(@conference)

            flash[:success_notice] = "Starša ste uspešno prijavili na govorilne ure."
            redirect_to 'teacher_home'

          else
            flash[:failed_notice] = "Za mesec #{month_id} in termin #{period}
            je že nekdo prijavljen k vam na govorilne ure. Izberite drug termin."

          end

        end
      else
        flash[:failed_notice] = "Prišlo je do napake pri vnosu podatkov. Preverite podatke in
        poskusite ponovno."
        redirect_to 'teacherbooking/edit'
      end
    end
  end
end
