module Web::Controllers::Teacherbooking
  class Edit
    include Web::Action

    before :check_for_teacher_class

    expose :months, :slot_values, :parent_email_values

    def call(params)
      @months = month_listbox_values
      
      # A hash of slot IDs and periods is created for the listbox
      # in the template.
      @slot_values = slot_listbox_values

      # A hash of parent IDs and emails is created for the listbox
      # in the template.
      @parent_email_values = parent_email_listbox_values
      
    end
  end
end
