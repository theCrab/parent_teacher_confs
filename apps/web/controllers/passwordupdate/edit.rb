module Web::Controllers::Passwordupdate
  class Edit
    include Web::Action

    expose :token

    def call(params)
      @token = params[:token]
    end
  end
end
