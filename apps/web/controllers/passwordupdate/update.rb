module Web::Controllers::Passwordupdate
  class Update
    include Web::Action

    params do
      param :token, presence: true
      param :passwordupdate do
        param :new_password, presence: true
        param :repeat_password, presence: true
      end
    end

    def call(params)
      #raise params[:token].inspect => RETURNS TOKEN
      #raise params[:passwordupdate][:new_password].inspect => RETURNS NEW PASSWORD
      if params.valid?
        @new_pass = params[:passwordupdate][:new_password]
        token = params[:token]
        
        # Find user by token, looking at both repositories.     
        @user = ParentRepository.user_by_token(token)
        
        if @user
          @repository = ParentRepository
        else
          @user = TeacherRepository.user_by_token(token)
          @repository = TeacherRepository
        end
        
        if Time.now > @user.password_reset_sent_at.to_time + 7200
          flash[:failed_notice] = "Veljavnost (2 uri) povezave za ponastavitev gesla je potekla."
          redirect_to "/"
        else
          password_salt = BCrypt::Engine.generate_salt
          password_hash = BCrypt::Engine.hash_secret(@new_pass, password_salt)
          
          @user.update(password_hash: password_hash, password_salt: password_salt)
          @user = @repository.update(@user)     
   
          flash[:success_notice] = "Geslo ste uspešno ponastavili."
          redirect_to "/"
        end
      else
        flash[:failed_notice] = "Prišlo je do napake pri vnosu podatkov. Preverite podatke in
        poskusite ponovno."
        redirect_to "/"
      end
    end
  end
end
