module Web::Controllers::ParentSessions
  class New
    include Web::Action

    # If a user visits the login page, they are first logged out.

    def call(params)
      session[:current_user] = nil
    end
  end
end
