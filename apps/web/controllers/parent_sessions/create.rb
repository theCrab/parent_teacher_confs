module Web::Controllers::ParentSessions
  class Create
    include Web::Action
    
    params do
      param :parent_session do
        param :email, presence: true, format: /\A(.*)@(\w*\d*)\.(\w*)\z/
        param :password, presence: true#, size: 8..64 ##This needs to be activated in pruduction.
      end
    end

    def call(params)
      if params.valid?
        email = params[:parent_session][:email]
        password = params[:parent_session][:password]
        
        # Parent is looked up in the repository by email
        @parent = ParentRepository.user_with_email(email)
        
        # Email and password (ad hoc encrypted) are compared with those
        # in the database and corresponding control flow is in place.
        
        if @parent && @parent.password_hash == BCrypt::Engine.hash_secret(password, @parent.password_salt)
          session[:current_user] = @parent
          flash[:success_notice] = "Uspešno ste se prijavili."
          redirect_to '/'
        else
          session[:current_user] = nil
          flash[:failed_notice] = "Vnesli ste napačen naslov e-pošte ali geslo."
          redirect_to '/parent_sessions/new'
        end
      else
        flash[:failed_notice] = "Prišlo je do napake pri vnosu podatkov. Preverite podatke in
        poskusite ponovno."
        redirect_to '/parent_sessions/new'
      end
    end
  end
end
