module Web::Controllers::Parents
  class Show
    include Web::Action

    before :check_for_admin_role

    expose :parent

    def call(params)
      @parent = ParentRepository.find(params[:id])
    end
  end
end
