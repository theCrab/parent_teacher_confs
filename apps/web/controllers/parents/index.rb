module Web::Controllers::Parents
  class Index
    include Web::Action

    before :check_for_admin_role

    expose :parents

    def call(params)
      @parents = ParentRepository.all
    end
  end
end
