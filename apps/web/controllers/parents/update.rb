module Web::Controllers::Parents
  class Update
    include Web::Action

    before :check_for_admin_role

    params do
      param :id, presence: true
      
      param :parent do
        param :name, presence: true, size: 3..20
        param :surname, presence: true, size: 3..20
        param :email, presence: true, format: /\A(.*)@(\w*\d*)\.(\w*)\z/
      end
    end

    def call(params)
      if params.valid?
        @parent = ParentRepository.find(params[:id])
        @parent.update(params[:parent])
        @parent = ParentRepository.update(@parent)
        flash[:success_notice] = "Starš je bil uspešno posodobljen."
        redirect_to routes.parent_url(id: @parent.id)
      else
        flash[:failed_notice] = "Prišlo je do napake pri vnosu podatkov. Preverite podatke in
        poskusite ponovno."
        redirect_to '/parents/edit'
      end
    end
  end
end
