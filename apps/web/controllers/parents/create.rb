module Web::Controllers::Parents
  class Create
    include Web::Action
    
    before :check_for_admin_role
    
    expose :parent
    
    params do
      param :parent do
        param :name, presence: true, size: 3..20
        param :surname, presence: true, size: 3..20
        param :email, presence: true, format: /\A[^@]*?@\w*?\.\w*\z/
        param :password, presence: true, confirmation: true#, size: 8..64 ##This needs to be activated in pruduction.
      end
    end
    
    def call(params)
      if params.valid?
        
        name = params[:parent][:name]
        surname = params[:parent][:surname]
        email = params[:parent][:email]
        password_salt = BCrypt::Engine.generate_salt
        password_hash = BCrypt::Engine.hash_secret(params[:parent][:password], password_salt)
        
        @parent = ParentRepository.create(Parent.new(name: name, surname: surname, email: email,
        password_hash: password_hash, password_salt: password_salt))
        flash[:success_notice] = "Starš je bil uspešno ustvarjen."
        redirect_to '/parents'
      else
        flash[:failed_notice] = "Prišlo je do napake pri vnosu podatkov. Preverite podatke in
        poskusite ponovno."
        redirect_to '/parents/new'
      end
    end    
  end
end
