module Web::Controllers::Parentcancel
  class Edit
    include Web::Action

    before :check_for_parent_class
      
      expose :teachers, :parent_conferences, :slots, :teacher_names

    def call(params)
      @teachers = TeacherRepository.all
      
      @parent_conferences = ConferenceRepository.parent_conferences(session[:current_user].id)
      
      
      # A hash of teacher IDs and teacher entities is created for the template.  
      @teacher_names = {}
      
      @parent_conferences.each {|p_conf|
        @teacher_names[p_conf.teacher_id] = TeacherRepository.find(p_conf.teacher_id)
        }
      
      
      # A hash of slot IDs and slot entites is created for the template.
      @slots = {}
      
      @parent_conferences.each {|p_conf|
        @slots[p_conf.slot_id] = SlotRepository.find(p_conf.slot_id).period
        }
      

    end
  end
end
