module Web::Controllers::ChangePtcDay
  class Update
    include Web::Action

    before :check_for_admin_role

    params do
      param :change_ptc_day do
        param :month_id, presence: true
        param :day, presence: true, type: Integer, size: 1..31
        param :teacher_id, presence: true
      end
    end

    def call(params)
      if params.valid?
        month_id = params[:change_ptc_day][:month_id]
        day = params[:change_ptc_day][:day]
        teacher_id = params[:change_ptc_day][:teacher_id]


        # Setup post for the day change
        title = "Sprememba dneva govorilnih ur"
        if teacher_id == "Izberi učitelja"
          body = "Dan govorilnih ur za mesec #{month} je spremenjen na #{day}."
          flash[:success_notice] = "Dan govorilnih ur za mesec #{month} je spremenjen na #{day}."

        else
          teacher = TeacherRepository.find(teacher_id)
          body = "Dan govorilnih ur za mesec #{month} in učitelja #{teacher.name} #{teacher.surname} je spremenjen na #{day}."
          flash[:success_notice] = "Dan govorilnih ur za mesec #{month} in učitelja #{teacher.name} #{teacher.surname} je spremenjen na #{day}."
        end

        published_at = Time.now

        @post = PostRepository.create(Post.new(title: title, body: body, published_at: published_at))


        # All conferences for the selected month are updated with the new day.
        confs_for_day_change = ConferenceRepository.conferences_for_day_change(month, teacher_id)

        relevant_parents = []

        # Get e-mails for all parents who are booked for the cancelled conferences
        confs_for_day_change.each {|conf|
          if conf.parent_id != 0
          parent = ParentRepository.find(conf.parent_id)
          relevant_parents << parent.email
          end
          }

        # Send email to all parents booked for the conferences that were cancelled
        if relevant_parents.any?
          Mailers::Daychangenotice.deliver(post_title: title, post_body: body, parents: relevant_parents)
        end


        confs_for_day_change.each {|conf|
          conf.update(day: day)
          conf = ConferenceRepository.update(conf)
          }

        redirect_to 'configuration'
      else
        flash[:failed_notice] = "Prišlo je do napake pri zamenjavi dneva govorilnih ur. Izberete lahko učitelja, mesec
        in dan, ali pa le mesec in dan."
        redirect_to 'change_ptc_day/edit'
      end
    end
  end
end
