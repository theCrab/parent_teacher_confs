module Web::Controllers::ChangePtcDay
  class Edit
    include Web::Action

    before :check_for_admin_role
    
    expose :months, :days, :teachers

    def call(params)
      
      @months = month_listbox_values
      @days = day_listbox_values
      @teachers = teacher_listbox_values
    end
  end
end
