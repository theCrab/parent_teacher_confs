module Web::Controllers::Months
  class Index
    include Web::Action

    before :check_for_admin_role

    expose :months

    def call(params)
      @months = MonthRepository.all
    end
  end
end
