module Web::Controllers::Months
  class Show
    include Web::Action

    before :check_for_admin_role

    expose :month

    def call(params)
      @month = MonthRepository.find(params[:id])
    end
  end
end
