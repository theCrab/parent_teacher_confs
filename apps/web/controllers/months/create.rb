module Web::Controllers::Months
  class Create
    include Web::Action

    before :check_for_admin_role

    params do
      param :month do
        param :name, presence: true
      end
    end

    expose :month

    def call(params)
      if params.valid?
        @month = MonthRepository.create(Month.new(params[:month]))
        flash[:success_notice] = "Mesec #{@month.name} je bil uspešno ustvarjen."
        redirect_to '/months'
      else
        flash[:failed_notice] = "Prišlo je do napake pri vnosu podatkov. Preverite podatke in
        poskusite ponovno."
        redirect_to '/months/new'
      end
    end
  end
end
