module Web::Controllers::Months
  class Update
    include Web::Action

    before :check_for_admin_role

    params do
      param :id, presence: true

      param :month do
        param :name, presence: true
      end
    end

    def call(params)
      if params.valid?
        @month = MonthRepository.find(params[:id])
        @month.update(params[:month])
        @month = MonthRepository.update(@month)

        flash[:success_notice] = "Mesec je bil uspešno posodobljen."
        redirect_to routes.month_url(id: @month.id)
      else
        flash[:failed_notice] = "Prišlo je do napake pri vnosu podatkov. Preverite podatke in
        poskusite ponovno."
        redirect_to '/months/edit'
      end
    end
  end
end
