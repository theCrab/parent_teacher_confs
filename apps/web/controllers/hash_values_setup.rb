module Web
  module HashValuesSetup

    # This module sets up different hashes to populate.
    # list boxes in templates.


    private

    def teacher_listbox_values
      teachers = TeacherRepository.all
      t_values = Hash.new
      t_values["Izberi učitelja"] = "Izberi učitelja"
      teachers.each {|teacher|
        t_values[teacher.id] = "#{teacher.name}, #{teacher.surname}"
      }
      t_values
    end

    def parent_listbox_values
      parents = ParentRepository.all
      p_values = Hash.new
      p_values["Izberi starša"] = "Izberi starša"
      parents.each {|parent|
        p_values[parent.id] = "#{parent.name}, #{parent.surname}"
      }
      p_values
    end

    def slot_listbox_values
      slots = SlotRepository.all
      slot_values = Hash.new
      slot_values["Izberi termin"] = "Izberi termin"
      slots.each {|slot|
        slot_values[slot.id] = "#{slot.period}"
      }
      slot_values
    end

    def month_listbox_values
      months = MonthRepository.all
      m_values = Hash.new
      m_values["Izberi mesec"] = "Izberi mesec"
      months.each {|month|
        m_values[month.id] = "#{month.name}" #If id here is changed to seq_no the parent_booking_conference_spec fails.
      }
      m_values
    end

    def conference_status_listbox_values
      {"Izberi status" => "Izberi status", "Aktiven" => "Aktiven", "Neaktiven" => "Neaktiven"}
    end

    def day_listbox_values
      day_values = {}
      day_values["Izberi dan"] = "Izberi dan"
      days = 1..31
      days.each {|day| day_values[day] = day }
      day_values
    end

    def parent_email_listbox_values
      parents = ParentRepository.all
      parent_email_values = {}
      parent_email_values["Izberi starša"] = "Izberi starša"

      parents.each {|parent|
        parent_email_values[parent.email] = "#{parent.email}"
      }
      parent_email_values
    end

    def booking_lock_listbox_values
      {"Spremeni status" => "Spremeni status", "izklopljeno" => "izklopljeno", "vklopljeno" => "vklopljeno"}
    end

    def months_by_numbers
      {1 => "Januar", 2 => "Februar", 3 => "Marec",
        4 => "April", 5 => "Maj", 6 => "Junij",
        9 => "September", 10 => "Oktober",
        11 =>"November", 12 => "December"}
    end

    def years_listbox_values
      {2016 => 2016, 2017 => 2017, 2018 => 2018, 2019 => 2019, 2020 => 2020}
    end

    def posts
      @posts = PostRepository.all.reverse
    end

    def recent_posts
      @recent_posts = PostRepository.recent_posts
    end

    def teacher_roles
      {"Izberi vlogo" => "Izberi vlogo", "teacher" => "učitelj", "administrator" => "administrator"}
    end

  end

end
