module Web::Controllers::Teachercancel
  class Update
    include Web::Action

    before :check_for_teacher_class

    params do
      param :teachercancel do
        param :pconferences, presence: true
      end
    end

    def call(params)
      if params.valid?      
        # Gathers all marked conferences from the template,
        # finds them in the DB and sets their respective
        # parent id to 0, effectively making them available
        # for booking again.
        
        @conferences_for_cancellation = params[:teachercancel][:pconferences]
        @conferences_for_cancellation.each {|conference|
          conference.to_i
          
          @conference = ConferenceRepository.find(conference)
          @conference.parent_id = 0
          @conference.update
          ConferenceRepository.update(@conference)
        }
        flash[:success_notice] = "Starša se uspešno odjavili od govorilnih ur."
        redirect_to '/teacher_home'
      else
        flash[:failed_notice] = "Prišlo je do napake pri vnosu podatkov. Preverite podatke in
        poskusite ponovno."
        redirect_to '/teachercancel/edit'
      end
    end
  end
end
