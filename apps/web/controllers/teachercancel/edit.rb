module Web::Controllers::Teachercancel
  class Edit
    include Web::Action

    before :check_for_teacher_class

    expose :teacher_conferences, :slots, :parent_names

    def call(params)
      @teacher_conferences = ConferenceRepository.teacher_conferences(session[:current_user].id)
      
      # A hash of parent IDs and parent entities is created for the template.  
      @parent_names = {}
      
      @teacher_conferences.each {|t_conf|
        @parent_names[t_conf.parent_id] = ParentRepository.find(t_conf.parent_id)
        }
      
      # A hash of slot IDs and slot entites is created for the template.     
      @slots = {}
      
      @teacher_conferences.each {|t_conf|
        @slots[t_conf.slot_id] = SlotRepository.find(t_conf.slot_id).period
        }
    end
  end
end
