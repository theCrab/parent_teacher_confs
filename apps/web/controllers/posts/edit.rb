module Web::Controllers::Posts
  class Edit
    include Web::Action

    before :check_for_admin_role
    
    expose :post

    def call(params)
      @post = PostRepository.find(params[:id])
    end
  end
end
