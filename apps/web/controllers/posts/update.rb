module Web::Controllers::Posts
  class Update
    include Web::Action

    before :check_for_admin_role

    params do
      param :id
      param :post do
        param :title, presence: true, size: 0..25
        param :body, presence: true, size: 0..250
      end
    end

    def call(params)
      if params.valid?
        @post = PostRepository.find(params[:id])# or halt 404
        
        title = params[:post][:title]
        body = params[:post][:body]
        published_at = Time.now
        
        @post.update(title: title, body: body, published_at: published_at)
        
        flash[:success_notice] = "Obvestilo je bilo uspešno posodobljeno."
        @post = PostRepository.update(@post)
        
        redirect_to routes.post_url(id: @post.id)
      
      else
        flash[:failed_notice] = "Prišlo je do napake pri vnosu podatkov. Preverite podatke in
        poskusite ponovno."
        redirect_to '/posts/edit'
      end
    end
  end
end
