module Web::Controllers::Posts
  class Create
    include Web::Action

    before :check_for_admin_role

    params do
      param :post do
        param :title, presence: true, size: 0..25
        param :body, presence: true, size: 0..250
      end
    end

    def call(params)
      if params.valid?

        title = params[:post][:title]
        body = params[:post][:body]
        published_at = Time.now

        @post = PostRepository.create(Post.new(title: title, body: body, published_at: published_at))
        flash[:success_notice] = "Obvestilo je bilo uspešno ustvarjeno."
        redirect_to '/posts'
      else
        flash[:failed_notice] = "Prišlo je do napake pri vnosu podatkov. Preverite podatke in
        poskusite ponovno."
        redirect_to '/posts/new'
      end
    end
  end
end
