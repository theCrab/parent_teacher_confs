module Web::Controllers::Posts
  class Index
    include Web::Action

    before :check_for_admin_role

    expose :posts

    def call(params)
      @posts = PostRepository.all
    end
  end
end
