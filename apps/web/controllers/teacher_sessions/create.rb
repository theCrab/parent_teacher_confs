module Web::Controllers::TeacherSessions
  class Create
    include Web::Action

    params do
      param :teacher_session do
        param :email, presence: true, format: /\A(.*)@(\w*\d*)\.(\w*)\z/
        param :password, presence: true#, size: 8..64 ##This needs to be activated in pruduction.
      end
    end

    def call(params)
      if params.valid?
        email = params[:teacher_session][:email]
        password = params[:teacher_session][:password]
        
        # Teacher is looked up in the repository by email
        @teacher = TeacherRepository.user_with_email(email)
        
        # Email and password (ad hoc encrypted) are compared with those
        # in the database and corresponding control flow is in place.
          
        if @teacher && @teacher.password_hash == BCrypt::Engine.hash_secret(password, @teacher.password_salt)
          session[:current_user] = @teacher
          flash[:success_notice] = "Uspešno ste se prijavili."
          redirect_to '/'
        else
          session[:current_user] = nil
          flash[:failed_notice] = "Vnesli ste napačen naslov e-pošte ali geslo."
          redirect_to '/teacher_sessions/new'
        end
      else
        flash[:failed_notice] = "Prišlo je do napake pri vnosu podatkov. Preverite podatke in
        poskusite ponovno."
        redirect_to '/teacher_sessions/new'
      end
    end
  end
end
