module Web::Controllers::DeaktTeacherConfday
  class Edit
    include Web::Action

    before :check_for_admin_role

    expose :months, :t_values

    def call(params)
      
      @t_values = teacher_listbox_values
      @months = month_listbox_values
      
    end
  end
end
