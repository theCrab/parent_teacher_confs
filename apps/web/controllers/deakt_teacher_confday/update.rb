module Web::Controllers::DeaktTeacherConfday
  class Update
    include Web::Action

    before :check_for_admin_role

    params do
      param :deakt_teacher_confday do
        param :month_id, presence: true
        param :teacher_id, presence: true
      end
    end

    def call(params)
      if params.valid?

        month_id = params[:deakt_teacher_confday][:month_id]
        teacher_id = params[:deakt_teacher_confday][:teacher_id]


        # Setup post for the reactivation
        title = "Govorilne ure so odpovedane"

        if teacher_id == "Izberi učitelja"
          body = "Govorilne ure za mesec #{month_id} so odpovedane."
        else
          teacher = TeacherRepository.find(teacher_id)
          body = "Govorilne ure za mesec #{month_id} in učitelja #{teacher.name} #{teacher.surname} so odpovedane."
        end
        published_at = Time.now

        @post = PostRepository.create(Post.new(title: title, body: body, published_at: published_at))


        # If teacher is not selected on the UI, all conferences for all
        # teachers are deactivated for a given month.

        conferences_for_cancel = ConferenceRepository.teacher_conferences_for_deactivation(month_id, teacher_id)
        relevant_parents = []

        # Get e-mails for all parents who are booked for the cancelled conferences
        conferences_for_cancel.each {|conf|
          if conf.parent_id != 0
          parent = ParentRepository.find(conf.parent_id)
          relevant_parents << parent.email
          end
          }

        # Send email to all parents booked for the conferences that were cancelled
        if relevant_parents.any?
          Mailers::Changenotification.deliver(post_title: title, post_body: body, parents: relevant_parents)
        end

        conferences_for_cancel.each {|conf|
          conf.update(status: "Neaktiven")
          conf = ConferenceRepository.update(conf)
          }
        flash[:success_notice] = "Govorilne ure so bile uspešno odpovedane."
        redirect_to 'configuration'
      else
        flash[:failed_notice] = "Prišlo je do napake pri vnosu podatkov. Preverite podatke in
        poskusite ponovno."
        redirect_to 'deakt_teacher_confday/edit'
      end
    end
  end
end
