module Web::Controllers::Slots
  class Create
    include Web::Action

    before :check_for_admin_role

    params do
      param :slot do
        param :period, presence: true, format: /(\d\d)\.(\d\d)\-(\d\d)\.(\d\d)/
      end  
    end

    expose :slot

    def call(params)
      if params.valid?
        @slot = SlotRepository.create(Slot.new(params[:slot]))
        flash[:success_notice] = "Termin je bil uspešno ustvarjen."
        redirect_to '/slots'
      else
        flash[:failed_notice] = "Prišlo je do napake pri vnosu podatkov. Preverite podatke in
        poskusite ponovno."
        redirect_to '/slots/new'
      end
    end
  end
end
