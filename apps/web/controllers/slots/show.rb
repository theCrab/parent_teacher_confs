module Web::Controllers::Slots
  class Show
    include Web::Action

    before :check_for_admin_role

    expose :slot

    def call(params)
      @slot = SlotRepository.find(params[:id])
    end
  end
end
