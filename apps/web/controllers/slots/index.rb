module Web::Controllers::Slots
  class Index
    include Web::Action

    before :check_for_admin_role

    expose :slots

    def call(params)
      @slots = SlotRepository.all
    end
  end
end
