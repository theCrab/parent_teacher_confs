module Web::Controllers::Slots
  class Update
    include Web::Action

    before :check_for_admin_role

    params do
      param :id, presence: true
      
      param :slot do
        param :period, presence: true, format: /(\d\d)\.(\d\d)\-(\d\d)\.(\d\d)/
      end
    end

    def call(params)
      if params.valid?
        @slot = SlotRepository.find(params[:id])
        @slot.update(params[:slot])
        @slot = SlotRepository.update(@slot)
        
        flash[:success_notice] = "Termin je bil uspešno posodobljen."
        redirect_to routes.slot_url(id: @slot.id)
      else
        flash[:failed_notice] = "Prišlo je do napake pri vnosu podatkov. Preverite podatke in
        poskusite ponovno."
        redirect_to '/slots/edit'
      end
    end
  end
end
