module Web::Controllers::Teachers
  class Edit
    include Web::Action

    before :check_for_admin_role

    expose :teacher, :t_roles

    def call(params)
      @teacher = TeacherRepository.find(params[:id])
      @t_roles = teacher_roles
    end
  end
end
