module Web::Controllers::Teachers
  class Index
    include Web::Action

    before :check_for_admin_role

    expose :teachers

    def call(params)
      @teachers = TeacherRepository.all
    end
  end
end
