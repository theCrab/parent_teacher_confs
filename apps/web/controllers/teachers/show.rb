module Web::Controllers::Teachers
  class Show
    include Web::Action

    before :check_for_admin_role

    expose :teacher

    def call(params)
      @teacher = TeacherRepository.find(params[:id])
    end
  end
end
