module Web::Controllers::Teachers
  class Create
    include Web::Action

    before :check_for_admin_role

    expose :teacher

    params do
      param :teacher do
        param :name, presence: true, size: 3..20
        param :surname, presence: true, size: 3..20
        param :email, presence: true, format: /\A[^@]*?@\w*?\.\w*\z/
        param :role, presence: true
        param :password, presence: true, confirmation: true#, size: 8..64 ##This needs to be activated in pruduction.
      end
    end

    def call(params)
      if params.valid?
        name = params[:teacher][:name]
        surname = params[:teacher][:surname]
        email = params[:teacher][:email]
        role = params[:teacher][:role]
        password_salt = BCrypt::Engine.generate_salt
        password_hash = BCrypt::Engine.hash_secret(params[:teacher][:password], password_salt)

        @teacher = TeacherRepository.create(Teacher.new(name: name, surname: surname, email: email,
        role: role, password_hash: password_hash, password_salt: password_salt))
        flash[:success_notice] = "Učitelj je bil uspešno ustvarjen."
        redirect_to '/teachers'
      else
        flash[:failed_notice] = "Prišlo je do napake pri vnosu podatkov. Preverite podatke in
        poskusite ponovno."
        redirect_to '/teachers/new'
      end
    end
  end
end
