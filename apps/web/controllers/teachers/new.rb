module Web::Controllers::Teachers
  class New
    include Web::Action

    before :check_for_admin_role

    expose :t_roles

    def call(params)
      @t_roles = teacher_roles
    end
  end
end
