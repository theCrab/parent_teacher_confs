module Web::Controllers::Teachers
  class Update
    include Web::Action

    before :check_for_admin_role

    params do
      param :id, presence: true
      
      param :teacher do
        param :name, presence: true, size: 3..20
        param :surname, presence: true, size: 3..20
        param :email, presence: true, format: /\A(.*)@(\w*\d*)\.(\w*)\z/
      end
    end

    def call(params)
      if params.valid?
        @teacher = TeacherRepository.find(params[:id])
        @teacher.update(params[:teacher])
        @teacher = TeacherRepository.update(@teacher)
        flash[:success_notice] = "Učitelj je bil uspešno posodobljen."
        redirect_to routes.teacher_url(id: @teacher.id)
      else
        flash[:failed_notice] = "Prišlo je do napake pri vnosu podatkov. Preverite podatke in
        poskusite ponovno."
        redirect_to '/teachers/edit'
      end
    end
  end
end
