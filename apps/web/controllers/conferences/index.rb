module Web::Controllers::Conferences
  class Index
    include Web::Action

    before :check_for_teacher_class

    expose :conferences, :teachers_values, :slot_values, :parents_values, :month_values

    def call(params)
      @conferences = ConferenceRepository.all

      @teachers_values = teacher_listbox_values

      @slot_values = slot_listbox_values

      @parents_values = parent_listbox_values

      @month_values = month_listbox_values

    end
  end
end
