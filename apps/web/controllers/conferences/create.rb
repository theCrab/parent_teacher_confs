module Web::Controllers::Conferences
  class Create
    include Web::Action

    before :check_for_admin_role

    expose :conference


    params do
      param :conference do
        param :month_id, presence: true
        param :day, presence: true
        param :status, presence: true
        param :teacher_id, presence: true
        param :slot_id, presence: true
        param :year, presence: true
      end
    end

    def call(params)
      if params.valid?
        month_id = params[:conference][:month_id]
        day = params[:conference][:day]
        status = params[:conference][:status]
        teacher_id = params[:conference][:teacher_id]
        slot_id = params[:conference][:slot_id]
        parent_id = 0
        year = params[:conference][:year]
        @month = MonthRepository.find(month_id)
        date = "#{day}. #{@month.seq_no}. #{year}"
        uid = "#{day}_#{@month.name}_#{year}_#{teacher_id}_#{slot_id}"

        @conference = ConferenceRepository.create(Conference.new(month_id: month_id, day: day, status: status,
                                                                 teacher_id: teacher_id, slot_id: slot_id,
                                                                 parent_id: parent_id, date: date, year: year,
                                                                 uid: uid))

        flash[:success_notice] = "Govorilne ure so bile uspešno ustvarjene."
        redirect_to '/conferences'
      else
        flash[:failed_notice] = "Prišlo je do napake pri vnosu podatkov. Preverite podatke in
        poskusite ponovno."
        redirect_to '/conferences/new'
      end
    end
  end
end
