module Web::Controllers::Conferences
  class New
    include Web::Action

    before :check_for_admin_role

    expose :t_values, :slot_values, :months, :statuses, :years

    def call(params)

      @statuses = conference_status_listbox_values

      @months = month_listbox_values

      # A hash of teacher IDs and full names is created for the listbox
      # in the template.
      @t_values = teacher_listbox_values


      # A hash of slot IDs and periods is created for the listbox
      # in the template.
      @slot_values = slot_listbox_values

      # A hash of years is created for the listbox
      # in the template.
      @years = years_listbox_values

    end
  end
end
