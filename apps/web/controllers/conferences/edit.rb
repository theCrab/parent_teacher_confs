module Web::Controllers::Conferences
  class Edit
    include Web::Action

    before :check_for_admin_role

      expose :conference, :t_values, :slot_values, :teacher_id, :slot_id, :p_values, :month_values, :years

    def call(params)
      @conference = ConferenceRepository.find(params[:id])

      # A hash of teacher IDs and full names is created for the listbox
      # in the template.
      @t_values = teacher_listbox_values

      # A hash of parent IDs and full names is created for the listbox
      # in the template.
      @p_values = parent_listbox_values


      # A hash of slot IDs and periods is created for the listbox
      # in the template.
      @slot_values = slot_listbox_values

      @teacher_id = @conference.teacher_id
      @slot_id = @conference.slot_id
      @month_values = month_listbox_values
      @years = years_listbox_values
    end
  end
end
