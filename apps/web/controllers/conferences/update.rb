module Web::Controllers::Conferences
  class Update
    include Web::Action

    before :check_for_admin_role

    params do
      param :id, presence: true
      param :conference do
        param :month_id, presence: true
        param :day, presence: true
        param :status, presence: true
        param :teacher_id, presence: true
        param :slot_id, presence: true
        param :parent_id, presence: true
      end
    end

    def call(params)
      if params.valid?
        @conference = ConferenceRepository.find(params[:id])
        @conference.update(params[:conference])
        @conference = ConferenceRepository.update(@conference)
        flash[:success_notice] = "Govorilne ure so bile uspešno posodobljene."
        redirect_to routes.conference_url(id: @conference.id)
      else
        flash[:failed_notice] = "Prišlo je do napake pri vnosu podatkov. Preverite podatke in
        poskusite ponovno."
        redirect_to 'conferences/edit'
      end
    end
  end
end
