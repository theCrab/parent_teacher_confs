module Web::Controllers::Conferences
  class Show
    include Web::Action

    before :check_for_teacher_class

    expose :conference, :teacher, :slot, :parent, :month_values

    def call(params)
      @conference = ConferenceRepository.find(params[:id])
      @teacher = TeacherRepository.find(@conference.teacher_id)
      @slot = SlotRepository.find(@conference.slot_id)
      @parent = ParentRepository.find(@conference.parent_id)
      @month_values = month_listbox_values
    end
  end
end
