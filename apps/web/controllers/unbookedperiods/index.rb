module Web::Controllers::Unbookedperiods
  class Index
    include Web::Action

    before :check_for_teacher_class

    expose :unbooked_conferences, :month, :teacher, :slot_values

    def call(params)
      # Get all unbooked conferences for the current month
      # and display their periods and the relevant
      # teacher.

      @teacher = session[:current_user]
      @slot_values = slot_listbox_values
      current_month = Time.now.month
      #@month = MonthRepository.find(current_month).name
      @month = months_by_numbers[current_month]
      @unbooked_conferences = ConferenceRepository.unbooked_conferences(@month, @teacher.id)

    end
  end
end
