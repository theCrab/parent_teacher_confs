module Web::Controllers::TeacherHome
  class Index
    include Web::Action

    before :check_for_teacher_class

    expose :teacher_conferences, :parents_names, :slots, :months


    def call(params)
      @teacher_conferences = ConferenceRepository.teacher_conferences(session[:current_user].id)

      @parents_names = {}

      @teacher_conferences.each {|t_conf|
        @parents_names[t_conf.parent_id] = ParentRepository.find(t_conf.parent_id)
        }

      @slots = {}
      @teacher_conferences.each {|t_conf|
        @slots[t_conf.slot_id] = SlotRepository.find(t_conf.slot_id).period
        }

        @months = {}

        @teacher_conferences.each {|t_conf|
          @months[t_conf.month_id] = MonthRepository.find(t_conf.month_id).name
        }

    end

  end
end
