module Web::Controllers::ParentBooking
  class Edit
    include Web::Action

    before :check_for_parent_class, :check_booking_lock

    expose :months, :t_values, :slot_values

    def call(params)
      @months = month_listbox_values
      
      # A hash of teacher IDs and full names is created for the listbox
      # in the template.
      @t_values = teacher_listbox_values

      
      # A hash of slot IDs and periods is created for the listbox
      # in the template.
      @slot_values = slot_listbox_values
      
    end
  end
end
