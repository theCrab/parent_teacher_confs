module Web::Controllers::ParentBooking
  class Update
    include Web::Action

    before :check_for_parent_class

    #params[:parent_id] is not needed as it's not a param being passed from the template.
    params do
      param :parent_booking do
        param :month_id, presence: true
        param :teacher_id, presence: true
        param :slot_id, presence: true
        param :parent_comment, size: 0..150
      end
    end

    def call(params)
      if params.valid?
        # Sets variables from param values
        month_id = params[:parent_booking][:month_id]
        teacher_id = params[:parent_booking][:teacher_id]
        slot_id = params[:parent_booking][:slot_id]
        comment = params[:parent_booking][:parent_comment]


        parent = session[:current_user].id
        teacher_name = TeacherRepository.find(teacher_id).name
        teacher_surname = TeacherRepository.find(teacher_id).surname
        period = SlotRepository.find(slot_id).period

        # Check for booked conferences in a month and conferences with a teacher in a month.
        booked_slots = ConferenceRepository.booked_slots_for_month(month_id, slot_id, parent)
        existing_booking = ConferenceRepository.check_if_teacher_month_booking_exists(month_id, teacher_id, parent)

        # Control flow based on above checks
        if existing_booking
          flash[:failed_notice] = "Za mesec #{month_id} ste se že prijavili k učitelju #{teacher_name} #{teacher_surname}."
          redirect_to 'parent_booking/edit'

        elsif booked_slots
          flash[:failed_notice] = "Za mesec #{month_id} ste že prijavljeni v terminu #{period}."
          redirect_to 'parent_booking/edit'

        else
          @conference = ConferenceRepository.conference_for_booking(month_id, teacher_id, slot_id)

          if @conference.parent_id == 0
            @conference.update(parent_id: parent, parent_comment: comment)
            @conference = ConferenceRepository.update(@conference)

            flash[:success_notice] = "Uspešno ste se prijavili na govorilne ure."
            redirect_to 'parent_home'

          else
            flash[:failed_notice] = "Za mesec #{month_id}, učitelja #{teacher_name} #{teacher_surname} in termin #{period}
            je že nekdo prijavljen. Izberite drug termin."

          end

        end

      else
        flash[:failed_notice] = "Prišlo je do napake pri vnosu podatkov. Preverite podatke in
        poskusite ponovno."
        redirect_to 'parent_booking/edit'
      end
    end

  end
end
