module Web
  module Authorization

    # This module activates different security checks.
    # If signed-in user has appropriate classes and/or role
    # they have the autorization to perform certain actions.


    def self.included(action)
      action.class_eval do
        before :check_traffic_light
      end
    end


    private

    def check_for_teacher_class
      unless ENV['HANAMI_ENV'] == 'test'
        halt 401 unless session[:current_user].class == Teacher
      end
    end

    def check_for_parent_class
      unless ENV['HANAMI_ENV'] == 'test'
        halt 401 unless session[:current_user].class == Parent
      end
    end

    def check_for_admin_role
      unless ENV['HANAMI_ENV'] == 'test'
        halt 401 unless session[:current_user].role == "administrator"
      end
    end

    def check_for_signed_in_user
      unless ENV['HANAMI_ENV'] == 'test'
        halt 401 unless session[:current_user]
      end
    end

    def check_booking_lock
      unless ENV['HANAMI_ENV'] == 'test'
        active_lock = SettingRepository.get_booking_lock
        if active_lock.value == "vklopljeno"
          flash[:failed_notice] = "Prijave na govorilne ure so zaprte."
          redirect_to "/parent_home"
        end
      end
    end

    def check_traffic_light
      unless ENV['HANAMI_ENV'] == 'test'
        active_lock = SettingRepository.get_booking_lock
        if active_lock.value == "vklopljeno"
          session['LOCK'] = "On"
        else
          session['LOCK'] = "Off"
        end
      end
    end

  end

end
