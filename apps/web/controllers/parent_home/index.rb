module Web::Controllers::ParentHome
  class Index
    include Web::Action

    before :check_for_parent_class

    expose :students, :parent_conferences, :slots, :teacher_names, :months

    def call(params)
      @parent_conferences = ConferenceRepository.parent_conferences(session[:current_user].id)

      @teacher_names = {}

      @parent_conferences.each {|p_conf|
        @teacher_names[p_conf.teacher_id] = TeacherRepository.find(p_conf.teacher_id)
      }

      @slots = {}

      @parent_conferences.each {|p_conf|
        @slots[p_conf.slot_id] = SlotRepository.find(p_conf.slot_id).period
      }

      @months = {}

      @parent_conferences.each {|p_conf|
        @months[p_conf.month_id] = MonthRepository.find(p_conf.month_id).name
      }

    end
  end
end
