module Web::Views::Freeconferences
  class Index
    include Web::View
    
    def render
      raw JSON.generate(periods)
    end
    
  end
end
