module Web::Views::Getday
  class Index
    include Web::View
    
    def render
      raw JSON.generate({day: "#{day}"})
    end
    
  end
end
