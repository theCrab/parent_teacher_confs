# Configure your routes here
# See: http://www.rubydoc.info/gems/hanami-router/#Usage
resources :parents
resources :conferences
resources :slots
resources :months
resources :teachers
resources :posts
resources :parent_sessions, only: [:new, :create]
resources :teacher_sessions, only: [:new, :create]
resources :settings, only: [:edit, :update]
resource :parentcancel, only: [:edit, :update]
resource :change_ptc_day, only: [:edit, :update]
resource :teacherbooking, only: [:edit, :update]
resource :teachercancel, only: [:edit, :update]
resource :deakt_teacher_confday, only: [:edit, :update]
resource :reakt_teacher_confday, only: [:edit, :update]
resource :temail, only: [:new, :create]
resource :passwordreset, only: [:edit, :update]
get '/', to: 'home#index'
get '/configuration', to: 'configuration#index'
get '/logout', to: 'logout#index'
get '/teacher_home', to: 'teacher_home#index'
get '/parent_home', to: 'parent_home#index'
get '/parent_booking/edit', to: 'parent_booking#edit'
patch '/parent_booking', to: 'parent_booking#update'
get '/freeconferences', to: 'freeconferences#index'
get '/getday', to: 'getday#index'
get '/instructions', to: 'instructions#index'
get '/passwordupdate/:token', token: /([^\/]+)/, to: 'passwordupdate#edit'
patch '/passwordupdate/:token', token: /([^\/]+)/, to: 'passwordupdate#update'
get '/unbookedperiods', to: 'unbookedperiods#index'
