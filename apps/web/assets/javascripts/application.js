$(document).ready(function() {
   $('.success-notice').delay(4000).fadeOut('slow');
});

$(document).ready(function() {
   $('.failed-notice').delay(5000).fadeOut('slow');
});


//Code for when parent is booking their own conference
$(document).ready(function() {
   var row = "<option value=\"" + "" + "\"selected='selected'>" + "" + "</option>";
   $(row).prependTo("#parent-booking-month-id")
   $(row).prependTo("#parent-booking-teacher-id")
});

$(document).ready(function() {
   $("#parent-booking-month-id").change(function(){
   var month_id = $(this).val()
    $.ajax({
                 url: "/getday", // Route to the Script Controller method
                type: "GET",
            dataType: "json",
                data: {month_id: month_id}, // This goes to Controller in params hash, i.e. params[:file_name]
            //complete: function() {},
             success: function(data) {
                        // Do something with the response here
                        console.log(data);
                        $.each(data, function(index, element) {
                           $("#conference_day").text(element)
                        });
                      },
               error: function() {
                        alert("Ajax error!")
                      }
    });
   });
});

$(document).ready(function() {
   $("#parent-booking-teacher-id").change(function(){
   var teacher_id = $("select#parent-booking-teacher-id").val()
   var month_id = $("#parent-booking-month-id").val()
    $.ajax({
                 url: "/freeconferences", // Route to the Script Controller method
                type: "GET",
            dataType: "json",
                data: {month_id: month_id, teacher_id: teacher_id}, // This goes to Controller in params hash, i.e. params[:file_name]
            //complete: function() {},
             success: function(data) {
                        // Do something with the response here
                        // Clear all options from sub category select
                        $("select#parent-booking-slot-id option").remove();
                        //put in a empty default line
                        var row = "<option value=\"" + "" + "\">" + "" + "</option>";
                        $(row).appendTo("select#parent-booking-slot-id");
                        // Fill sub category select
                        $.each(data, function(i, j){
                            row = "<option value=\"" + i + "\">" + j + "</option>";
                            $(row).appendTo("select#parent-booking-slot-id");
                        });
                        console.log(data);
                      },
               error: function() {
                        alert("Ajax error!")
                      }
    });
   });
});


//Code for teachers booking a conference for a parent
$(document).ready(function() {
   var row = "<option value=\"" + "" + "\"selected='selected'>" + "" + "</option>";
   $(row).prependTo("#teacherbooking-month-id")
});

$(document).ready(function() {
   $("#teacherbooking-month-id").change(function(){
   var month_id = $(this).val()
    $.ajax({
                 url: "/getday", // Route to the Script Controller method
                type: "GET",
            dataType: "json",
                data: {month_id: month_id}, // This goes to Controller in params hash, i.e. params[:file_name]
            //complete: function() {},
             success: function(data) {
                        // Do something with the response here
                        console.log(data);
                        $.each(data, function(index, element) {
                           $("#conference_day").text(element)
                        });
                      },
               error: function() {
                        alert("Ajax error!")
                      }
    });
   });
});

$(document).ready(function() {
   $("#teacherbooking-month-id").change(function(){
   var teacher_id = $("#teacher").val()
   var month_id = $("#teacherbooking-month-id").val()
    $.ajax({
                 url: "/freeconferences", // Route to the Script Controller method
                type: "GET",
            dataType: "json",
                data: {month_id: month_id, teacher_id: teacher_id}, // This goes to Controller in params hash, i.e. params[:file_name]
            //complete: function() {},
             success: function(data) {
                        // Do something with the response here
                        // Clear all options from sub category select
                        $("select#teacherbooking-slot-id option").remove();
                        //put in a empty default line
                        var row = "<option value=\"" + "" + "\">" + "" + "</option>";
                        $(row).appendTo("select#teacherbooking-slot-id");
                        // Fill sub category select
                        $.each(data, function(i, j){
                            row = "<option value=\"" + i + "\">" + j + "</option>";
                            $(row).appendTo("select#teacherbooking-slot-id");
                        });
                        console.log(data);
                      },
               error: function() {
                        alert("Ajax error!")
                      }
    });
   });
});

//Script for toggling the display of recent and all posts
$(document).ready(function() {

            $('#recent_posts_button').click(function(){
              $('#recent_posts').show(600);
              $('#all_posts_home').hide();
            });

            $('#all_posts_button').click(function(){
              $('#recent_posts').hide(600);
              $('#all_posts_home').show(600);
            });
});


//Script for modal dialogs for confirmations
$(document).ready(function() {

    $('.dialog-confirm').on('click', function(e){
        e.preventDefault();
        $('#dialog').dialog('open');
    });

    $('#dialog').dialog({
        autoOpen: false,
        modal: true,
        buttons: {
            "Da": function(e) {
                $(this).dialog('close');
                $('#formCheck').submit();


            },
            "Ne": function() {
                $(this).dialog('close');
            }
        }
    });
});

//Script for the burger icon when navbar is minimized
$(document).ready(function() {
  var x = document.getElementById("hidden_burger");
    $('#my_burger').on('click', function(e){
      if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
      }
      else {
        x.className = x.className.replace(" w3-show", "");
      }
    });
});
