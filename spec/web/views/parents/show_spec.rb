require 'spec_helper'
require_relative '../../../../apps/web/views/parents/show'

describe Web::Views::Parents::Show do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/parents/show.html.erb') }
  let(:view)      { Web::Views::Parents::Show.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
