require 'spec_helper'
require_relative '../../../../apps/web/views/parents/update'

describe Web::Views::Parents::Update do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/parents/update.html.erb') }
  let(:view)      { Web::Views::Parents::Update.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
