require 'spec_helper'
require_relative '../../../../apps/web/views/parents/new'

describe Web::Views::Parents::New do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/parents/new.html.erb') }
  let(:view)      { Web::Views::Parents::New.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
