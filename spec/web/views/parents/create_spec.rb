require 'spec_helper'
require_relative '../../../../apps/web/views/parents/create'

describe Web::Views::Parents::Create do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/parents/create.html.erb') }
  let(:view)      { Web::Views::Parents::Create.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
