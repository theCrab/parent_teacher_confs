require 'spec_helper'
require_relative '../../../../apps/web/views/parents/edit'

describe Web::Views::Parents::Edit do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/parents/edit.html.erb') }
  let(:view)      { Web::Views::Parents::Edit.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
