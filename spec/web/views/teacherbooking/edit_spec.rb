require 'spec_helper'
require_relative '../../../../apps/web/views/teacherbooking/edit'

describe Web::Views::Teacherbooking::Edit do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/teacherbooking/edit.html.erb') }
  let(:view)      { Web::Views::Teacherbooking::Edit.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
