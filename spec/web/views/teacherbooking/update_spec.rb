require 'spec_helper'
require_relative '../../../../apps/web/views/teacherbooking/update'

describe Web::Views::Teacherbooking::Update do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/teacherbooking/update.html.erb') }
  let(:view)      { Web::Views::Teacherbooking::Update.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
