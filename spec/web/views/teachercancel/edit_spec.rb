require 'spec_helper'
require_relative '../../../../apps/web/views/teachercancel/edit'

describe Web::Views::Teachercancel::Edit do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/teachercancel/edit.html.erb') }
  let(:view)      { Web::Views::Teachercancel::Edit.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
