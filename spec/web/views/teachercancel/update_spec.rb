require 'spec_helper'
require_relative '../../../../apps/web/views/teachercancel/update'

describe Web::Views::Teachercancel::Update do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/teachercancel/update.html.erb') }
  let(:view)      { Web::Views::Teachercancel::Update.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
