require 'spec_helper'
require_relative '../../../../apps/web/views/getday/index'

describe Web::Views::Getday::Index do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/getday/index.html.erb') }
  let(:view)      { Web::Views::Getday::Index.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
