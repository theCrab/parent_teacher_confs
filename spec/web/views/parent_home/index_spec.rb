require 'spec_helper'
require_relative '../../../../apps/web/views/parent_home/index'

describe Web::Views::ParentHome::Index do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/parent_home/index.html.erb') }
  let(:view)      { Web::Views::ParentHome::Index.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
