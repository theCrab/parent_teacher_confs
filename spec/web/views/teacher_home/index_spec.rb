require 'spec_helper'
require_relative '../../../../apps/web/views/teacher_home/index'

describe Web::Views::TeacherHome::Index do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/teacher_home/index.html.erb') }
  let(:view)      { Web::Views::TeacherHome::Index.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
