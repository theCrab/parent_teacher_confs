require 'spec_helper'
require_relative '../../../../apps/web/views/reakt_teacher_confday/update'

describe Web::Views::ReaktTeacherConfday::Update do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/reakt_teacher_confday/update.html.erb') }
  let(:view)      { Web::Views::ReaktTeacherConfday::Update.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
