require 'spec_helper'
require_relative '../../../../apps/web/views/temail/new'

describe Web::Views::Temail::New do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/temail/new.html.erb') }
  let(:view)      { Web::Views::Temail::New.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
