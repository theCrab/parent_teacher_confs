require 'spec_helper'
require_relative '../../../../apps/web/views/temail/create'

describe Web::Views::Temail::Create do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/temail/create.html.erb') }
  let(:view)      { Web::Views::Temail::Create.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
