require 'spec_helper'
require_relative '../../../../apps/web/views/settings/edit'

describe Web::Views::Settings::Edit do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/settings/edit.html.erb') }
  let(:view)      { Web::Views::Settings::Edit.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
