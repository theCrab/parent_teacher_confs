require 'spec_helper'
require_relative '../../../../apps/web/views/settings/update'

describe Web::Views::Settings::Update do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/settings/update.html.erb') }
  let(:view)      { Web::Views::Settings::Update.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
