require 'spec_helper'
require_relative '../../../../apps/web/views/change_ptc_day/update'

describe Web::Views::ChangePtcDay::Update do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/change_ptc_day/update.html.erb') }
  let(:view)      { Web::Views::ChangePtcDay::Update.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
