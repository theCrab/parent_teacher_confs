require 'spec_helper'
require_relative '../../../../apps/web/views/change_ptc_day/edit'

describe Web::Views::ChangePtcDay::Edit do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/change_ptc_day/edit.html.erb') }
  let(:view)      { Web::Views::ChangePtcDay::Edit.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
