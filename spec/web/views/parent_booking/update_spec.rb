require 'spec_helper'
require_relative '../../../../apps/web/views/parent_booking/update'

describe Web::Views::ParentBooking::Update do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/parent_booking/update.html.erb') }
  let(:view)      { Web::Views::ParentBooking::Update.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
