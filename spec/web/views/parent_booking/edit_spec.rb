require 'spec_helper'
require_relative '../../../../apps/web/views/parent_booking/edit'

describe Web::Views::ParentBooking::Edit do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/parent_booking/edit.html.erb') }
  let(:view)      { Web::Views::ParentBooking::Edit.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
