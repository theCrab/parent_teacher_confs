require 'spec_helper'
require_relative '../../../../apps/web/views/configuration/index'

describe Web::Views::Configuration::Index do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/configuration/index.html.erb') }
  let(:view)      { Web::Views::Configuration::Index.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
