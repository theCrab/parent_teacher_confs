require 'spec_helper'
require_relative '../../../../apps/web/views/parentcancel/update'

describe Web::Views::Parentcancel::Update do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/parentcancel/update.html.erb') }
  let(:view)      { Web::Views::Parentcancel::Update.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
