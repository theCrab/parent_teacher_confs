require 'spec_helper'
require_relative '../../../../apps/web/views/posts/update'

describe Web::Views::Posts::Update do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/posts/update.html.erb') }
  let(:view)      { Web::Views::Posts::Update.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
