require 'spec_helper'
require_relative '../../../../apps/web/views/teachers/create'

describe Web::Views::Teachers::Create do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/teachers/create.html.erb') }
  let(:view)      { Web::Views::Teachers::Create.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
