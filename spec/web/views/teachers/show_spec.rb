require 'spec_helper'
require_relative '../../../../apps/web/views/teachers/show'

describe Web::Views::Teachers::Show do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/teachers/show.html.erb') }
  let(:view)      { Web::Views::Teachers::Show.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
