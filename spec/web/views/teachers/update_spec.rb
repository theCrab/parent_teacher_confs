require 'spec_helper'
require_relative '../../../../apps/web/views/teachers/update'

describe Web::Views::Teachers::Update do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/teachers/update.html.erb') }
  let(:view)      { Web::Views::Teachers::Update.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
