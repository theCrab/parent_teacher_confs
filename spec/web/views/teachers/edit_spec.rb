require 'spec_helper'
require_relative '../../../../apps/web/views/teachers/edit'

describe Web::Views::Teachers::Edit do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/teachers/edit.html.erb') }
  let(:view)      { Web::Views::Teachers::Edit.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
