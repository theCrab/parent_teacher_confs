require 'spec_helper'
require_relative '../../../../apps/web/views/passwordreset/edit'

describe Web::Views::Passwordreset::Edit do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/passwordreset/edit.html.erb') }
  let(:view)      { Web::Views::Passwordreset::Edit.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
