require 'spec_helper'
require_relative '../../../../apps/web/views/instructions/index'

describe Web::Views::Instructions::Index do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/instructions/index.html.erb') }
  let(:view)      { Web::Views::Instructions::Index.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
