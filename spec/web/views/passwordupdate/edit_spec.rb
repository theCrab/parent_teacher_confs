require 'spec_helper'
require_relative '../../../../apps/web/views/passwordupdate/edit'

describe Web::Views::Passwordupdate::Edit do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/passwordupdate/edit.html.erb') }
  let(:view)      { Web::Views::Passwordupdate::Edit.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
