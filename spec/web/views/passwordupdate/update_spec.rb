require 'spec_helper'
require_relative '../../../../apps/web/views/passwordupdate/update'

describe Web::Views::Passwordupdate::Update do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/passwordupdate/update.html.erb') }
  let(:view)      { Web::Views::Passwordupdate::Update.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
