require 'spec_helper'
require_relative '../../../../apps/web/views/teacher_sessions/new'

describe Web::Views::TeacherSessions::New do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/teacher_sessions/new.html.erb') }
  let(:view)      { Web::Views::TeacherSessions::New.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
