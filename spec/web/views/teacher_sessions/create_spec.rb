require 'spec_helper'
require_relative '../../../../apps/web/views/teacher_sessions/create'

describe Web::Views::TeacherSessions::Create do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/teacher_sessions/create.html.erb') }
  let(:view)      { Web::Views::TeacherSessions::Create.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
