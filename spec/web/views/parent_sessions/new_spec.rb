require 'spec_helper'
require_relative '../../../../apps/web/views/parent_sessions/new'

describe Web::Views::ParentSessions::New do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/parent_sessions/new.html.erb') }
  let(:view)      { Web::Views::ParentSessions::New.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
