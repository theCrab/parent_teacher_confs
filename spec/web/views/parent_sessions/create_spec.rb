require 'spec_helper'
require_relative '../../../../apps/web/views/parent_sessions/create'

describe Web::Views::ParentSessions::Create do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/parent_sessions/create.html.erb') }
  let(:view)      { Web::Views::ParentSessions::Create.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
