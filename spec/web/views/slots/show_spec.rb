require 'spec_helper'
require_relative '../../../../apps/web/views/slots/show'

describe Web::Views::Slots::Show do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/slots/show.html.erb') }
  let(:view)      { Web::Views::Slots::Show.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
