require 'spec_helper'
require_relative '../../../../apps/web/views/slots/create'

describe Web::Views::Slots::Create do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/slots/create.html.erb') }
  let(:view)      { Web::Views::Slots::Create.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
