require 'spec_helper'
require_relative '../../../../apps/web/views/slots/update'

describe Web::Views::Slots::Update do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/slots/update.html.erb') }
  let(:view)      { Web::Views::Slots::Update.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
