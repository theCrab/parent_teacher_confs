require 'spec_helper'
require_relative '../../../../apps/web/views/slots/new'

describe Web::Views::Slots::New do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/slots/new.html.erb') }
  let(:view)      { Web::Views::Slots::New.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
