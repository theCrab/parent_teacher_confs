require 'spec_helper'
require_relative '../../../../apps/web/views/slots/edit'

describe Web::Views::Slots::Edit do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/slots/edit.html.erb') }
  let(:view)      { Web::Views::Slots::Edit.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
