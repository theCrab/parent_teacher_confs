require 'spec_helper'
require_relative '../../../../apps/web/views/months/edit'

describe Web::Views::Months::Edit do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/months/edit.html.erb') }
  let(:view)      { Web::Views::Months::Edit.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
