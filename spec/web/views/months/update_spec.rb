require 'spec_helper'
require_relative '../../../../apps/web/views/months/update'

describe Web::Views::Months::Update do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/months/update.html.erb') }
  let(:view)      { Web::Views::Months::Update.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
