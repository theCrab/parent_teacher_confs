require 'spec_helper'
require_relative '../../../../apps/web/views/months/show'

describe Web::Views::Months::Show do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/months/show.html.erb') }
  let(:view)      { Web::Views::Months::Show.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
