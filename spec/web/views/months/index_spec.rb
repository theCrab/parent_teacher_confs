require 'spec_helper'
require_relative '../../../../apps/web/views/months/index'

describe Web::Views::Months::Index do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/months/index.html.erb') }
  let(:view)      { Web::Views::Months::Index.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
