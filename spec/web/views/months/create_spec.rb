require 'spec_helper'
require_relative '../../../../apps/web/views/months/create'

describe Web::Views::Months::Create do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/months/create.html.erb') }
  let(:view)      { Web::Views::Months::Create.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
