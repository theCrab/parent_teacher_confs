require 'spec_helper'
require_relative '../../../../apps/web/views/months/new'

describe Web::Views::Months::New do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/months/new.html.erb') }
  let(:view)      { Web::Views::Months::New.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
