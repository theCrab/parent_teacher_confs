require 'spec_helper'
require_relative '../../../../apps/web/views/conferences/new'

describe Web::Views::Conferences::New do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/conferences/new.html.erb') }
  let(:view)      { Web::Views::Conferences::New.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
