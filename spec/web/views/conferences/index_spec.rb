require 'spec_helper'
require_relative '../../../../apps/web/views/conferences/index'

describe Web::Views::Conferences::Index do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/conferences/index.html.erb') }
  let(:view)      { Web::Views::Conferences::Index.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
