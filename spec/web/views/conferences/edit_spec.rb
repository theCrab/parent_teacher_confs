require 'spec_helper'
require_relative '../../../../apps/web/views/conferences/edit'

describe Web::Views::Conferences::Edit do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/conferences/edit.html.erb') }
  let(:view)      { Web::Views::Conferences::Edit.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
