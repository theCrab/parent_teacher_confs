require 'spec_helper'
require_relative '../../../../apps/web/views/conferences/show'

describe Web::Views::Conferences::Show do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/conferences/show.html.erb') }
  let(:view)      { Web::Views::Conferences::Show.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
