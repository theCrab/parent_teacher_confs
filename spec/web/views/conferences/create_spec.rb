require 'spec_helper'
require_relative '../../../../apps/web/views/conferences/create'

describe Web::Views::Conferences::Create do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/conferences/create.html.erb') }
  let(:view)      { Web::Views::Conferences::Create.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
