require 'spec_helper'
require_relative '../../../../apps/web/views/conferences/update'

describe Web::Views::Conferences::Update do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/conferences/update.html.erb') }
  let(:view)      { Web::Views::Conferences::Update.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
