require 'spec_helper'
require_relative '../../../../apps/web/views/deakt_teacher_confday/update'

describe Web::Views::DeaktTeacherConfday::Update do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/deakt_teacher_confday/update.html.erb') }
  let(:view)      { Web::Views::DeaktTeacherConfday::Update.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #foo' do
    view.foo.must_equal exposures.fetch(:foo)
  end
end
