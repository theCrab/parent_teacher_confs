require 'features_helper'

describe 'Parent books a conference' do

  before do
    t_password_salt = BCrypt::Engine.generate_salt
    t_password_hash = BCrypt::Engine.hash_secret("456", t_password_salt)
    p_password_salt = BCrypt::Engine.generate_salt
    p_password_hash = BCrypt::Engine.hash_secret("123", p_password_salt)

    @teacher = TeacherRepository.create(Teacher.new(name: 'Great', surname: 'Teacher', email: 'some1@mail.com',
    role: 'teacher', password_hash: t_password_hash, password_salt: t_password_salt))

    @parent = ParentRepository.create(Parent.new(name: 'Zigy', surname: 'Biggy', email: '345333wer@bla.com',
    password_hash: p_password_hash, password_salt: p_password_salt))

    @slot = SlotRepository.create(Slot.new(period: '17.00-17.10'))

    @month = MonthRepository.create(Month.new(name: 'Januar', seq_no: 1))

    @date = "12. #{@month.seq_no}. 2016"

    @uid = "12_#{@month.name}_2016_#{@teacher.id}_#{@slot.id}"

    @conference = ConferenceRepository.create(Conference.new(month_id: @month.id, day: 12, status: 'Aktiven',
                                                                 teacher_id: @teacher.id, slot_id: @slot.id,
                                                                 parent_id: 0, date: @date, year: 2016,
                                                                 uid: @uid))
  end

  after do
    ConferenceRepository.clear
    TeacherRepository.clear
    ParentRepository.clear
    SlotRepository.clear
    MonthRepository.clear
  end


  it 'updates parent id' do
    @conference.id.wont_be_nil

    visit "/parent_sessions/new"
    fill_in('parent_session[email]', with: @parent.email)
    fill_in('parent_session[password]', with: '123')

    click_button('Potrdi')


    visit "/parent_booking/edit"
    current_path.must_equal("/parent_booking/edit")

    # Add selects here
    select('Januar', :from => 'parent_booking[month_id]')
    select('Great, Teacher', :from => 'parent_booking[teacher_id]')
    select('17.00-17.10', :from => 'parent_booking[slot_id]')
    fill_in('parent-booking-parent-comment', with: 'Testno sporočilo')

    click_button('Potrdi')
    
    assert current_path.must_equal("/parent_home")

    assert page.has_content?('Great'), "Page must have content of Great."
    assert page.has_content?('Testno sporočilo'), "Page must have content of Testno sporočilo."
  end

end
