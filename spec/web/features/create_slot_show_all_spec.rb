require 'features_helper'

describe 'Create a new slot' do
  after do
    SlotRepository.clear
  end
  
  it 'Creates a new slot' do
    
    visit '/slots/new'
    fill_in('slot[period]', with: "16.00-16.10")
    
    click_button('Potrdi')
    current_path.must_equal('/slots')
    assert page.has_content?('16.00-16.10'), "Page must have content 16.00-16.10"
  end
  
end