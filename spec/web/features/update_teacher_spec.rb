require 'features_helper'

describe 'Edit and update teacher' do

  before do
    t_password_salt = BCrypt::Engine.generate_salt
    t_password_hash = BCrypt::Engine.hash_secret("456", t_password_salt)
    @teacher = TeacherRepository.create(Teacher.new(name: 'Great', surname: 'Teacher', email: 'some1@mail.com',
    role: 'teacher', password_hash: t_password_hash, password_salt: t_password_salt))
  end

  after do
    TeacherRepository.clear
  end

  it 'updates teacher data' do
    @teacher.id.wont_be_nil
    visit "/teachers/#{@teacher.id}/edit"
    current_path.must_equal("/teachers/#{@teacher.id}/edit")


    fill_in('teacher[name]', with: 'BubiBubi')
    click_button('Potrdi')
    current_path.must_equal("/teachers/#{@teacher.id}")
    assert page.has_content?('BubiBubi'), "The updated teacher name should be 'BubiBubi'."
  end

end
