require 'features_helper'

describe 'Edit and update slot' do

  before do
    @slot = SlotRepository.create(Slot.new(period: '17.00-17.10'))
  end

  after do
    SlotRepository.clear
  end


  it 'updates slot data' do
    @slot.id.wont_be_nil
    visit "/slots/#{@slot.id}/edit"
    current_path.must_equal("/slots/#{@slot.id}/edit")


    fill_in('slot[period]', with: '16.00-16.10')
    click_button('Potrdi')
    current_path.must_equal("/slots/#{@slot.id}")
    assert page.has_content?('16.00-16.10'), "The updated slot period should be '16.00-16.10'."
  end

end
