require 'features_helper'

describe 'Edit and update conference' do

  before do
    t_password_salt = BCrypt::Engine.generate_salt
    t_password_hash = BCrypt::Engine.hash_secret("456", t_password_salt)
    p_password_salt = BCrypt::Engine.generate_salt
    p_password_hash = BCrypt::Engine.hash_secret("123", p_password_salt)

    @teacher = TeacherRepository.create(Teacher.new(name: 'Great', surname: 'Teacher', email: 'some1@mail.com',
    role: 'teacher', password_hash: t_password_hash, password_salt: t_password_salt))

    @parent = ParentRepository.create(Parent.new(name: 'Zigy', surname: 'Biggy', email: '345333wer@bla.com',
    password_hash: p_password_hash, password_salt: p_password_salt))

    @slot = SlotRepository.create(Slot.new(period: '17.00-17.10'))

    @month = MonthRepository.create(Month.new(name: 'Januar', seq_no: 1))

    @date = "12. #{@month.seq_no}. 2016"

    @uid = "12_#{@month.name}_2016_#{@teacher.id}_#{@slot.id}"

    @conference = ConferenceRepository.create(Conference.new(month_id: @month.id, day: 12, status: 'Aktiven',
                                                             teacher_id: @teacher.id, slot_id: @slot.id,
                                                             parent_id: 0, date: @date, year: 2016,
                                                             uid: @uid))
  end

  after do
    ConferenceRepository.clear
    TeacherRepository.clear
    ParentRepository.clear
    SlotRepository.clear
    MonthRepository.clear
  end


  it 'updates conference data' do
    visit "/conferences/#{@conference.id}/edit"
    assert page.has_content?("Januar"), "Page must have content Januar."
    assert page.has_content?("Great, Teacher"), "Page must have content Great, Teacher."
    assert page.has_content?("#{@slot.period}"), "Page must have content #{@slot.period}."
    assert page.has_content?("12. 1. 2016"), "Page must have content 12. 1. 2016."
    assert page.has_content?("Leto"), "Page must have content Leto."
    assert page.has_content?("2016"), "Page must have content 2016."
    assert page.has_content?("Enoznačna oznaka"), "Page must have content Enoznačna oznaka."
    assert page.has_content?("#{@uid}"), "Page must have content #{@uid}."


    fill_in('conference[day]', with: 12)
    select('Januar', :from => 'conference-month-id')
    select('Great, Teacher', :from => 'conference-teacher-id')
    select('Zigy, Biggy', :from => 'conference-parent-id')
    select('17.00-17.10', :from => 'conference-slot-id')
    fill_in('conference[status]', with: 'Aktiven')
    click_button('Potrdi')

    current_path.must_equal("/conferences/#{@conference.id}")
    assert page.has_content?("Zigy, Biggy"), "Page must have content Zigy, Biggy."
  end

end
