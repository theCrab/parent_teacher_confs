require 'features_helper'

describe 'Test configuration links' do

  it 'links to new parent' do
    visit '/configuration'
    click_link('Ustvari starša')
    current_path.must_equal('/parents/new')
  end

    it 'links to all parents' do
    visit '/configuration'
    click_link('Prikaži vse starše')
    current_path.must_equal('/parents')
  end

  it 'links to new teacher' do
    visit '/configuration'
    click_link('Ustvari učitelja')
    current_path.must_equal('/teachers/new')
  end

  it 'links to all teachers' do
    visit '/configuration'
    click_link('Prikaži vse učitelje')
    current_path.must_equal('/teachers')
  end

  it "links to all months" do
    visit '/configuration'
    click_link('Prikaži vse mesece')
    current_path.must_equal('/months')
  end

  it "links to a new month" do
    visit '/configuration'
    click_link('Ustvari mesec')
    current_path.must_equal('/months/new')
  end

  it 'links to new slot' do
    visit '/configuration'
    click_link('Ustvari termin')
    current_path.must_equal('/slots/new')
  end

  it 'links to all slots' do
    visit '/configuration'
    click_link('Prikaži vse termine')
    current_path.must_equal('/slots')
  end

  it 'links to new conference' do
    visit '/configuration'
    click_link('Ustvari govorilne ure')
    current_path.must_equal('/conferences/new')
  end

  it 'links to all conferences' do
    visit '/configuration'
    click_link('Prikaži vse govorilne ure')
    current_path.must_equal('/conferences')
  end

end
