require 'features_helper'

describe 'Edit and update parent' do

  before do
    p_password_salt = BCrypt::Engine.generate_salt
    p_password_hash = BCrypt::Engine.hash_secret("123", p_password_salt)
    @parent = ParentRepository.create(Parent.new(name: 'Zigy', surname: 'Biggy', email: '345333wer@bla.com',
    password_hash: p_password_hash, password_salt: p_password_salt))
  end

  after do
    ParentRepository.clear
  end


  it 'updates parent data' do
    visit "/parents/#{@parent.id}/edit"
    current_path.must_equal("/parents/#{@parent.id}/edit")

    fill_in('parent[name]', with: "BubiBubi")
    click_button('Potrdi')
    current_path.must_equal("/parents/#{@parent.id}")
    assert page.has_content?("BubiBubi"), "The updated parent name should be BubiBubi."
  end

end
