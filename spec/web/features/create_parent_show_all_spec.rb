require 'features_helper'

describe 'Create a new parent' do
  after do
    ParentRepository.clear
  end
  
  it 'Creates a new parent' do
    
    visit '/parents/new'
    fill_in('parent[name]', with: "Bebo")
    fill_in('parent[surname]', with: "Tester")
    fill_in('parent[email]', with: "bebo_123@drek1.sem")
    fill_in('parent[password]', with: "123qwe1")
    fill_in('parent[password_confirmation]', with: "123qwe1")
    
    
    click_button('Potrdi')
    current_path.must_equal('/parents')
    assert page.has_content?('Tester'), "Page must have content Tester"
  end
  
end