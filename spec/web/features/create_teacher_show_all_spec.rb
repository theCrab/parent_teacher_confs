require 'features_helper'

describe 'Create a new teacher' do
  after do
    TeacherRepository.clear
  end

  it 'Creates a new teacher' do

    visit '/teachers/new'
    fill_in('teacher[name]', with: "Bebo")
    fill_in('teacher[surname]', with: "TesterUčitelj")
    fill_in('teacher[email]', with: "bebo_123@mail.com")
    fill_in('teacher[password]', with: "123qwe")
    fill_in('teacher[password_confirmation]', with: "123qwe")
    select('učitelj', :from => 'teacher[role]')

    click_button('Potrdi')
    current_path.must_equal('/teachers')
    assert page.has_content?('TesterUčitelj'), "Page must have content TesterUčitelj"
  end

end
