require 'spec_helper'
require_relative '../../../../apps/web/controllers/slots/show'

describe Web::Controllers::Slots::Show do
  let(:action) { Web::Controllers::Slots::Show.new }
  let(:params) { Hash[] }

  it 'is successful' do
    response = action.call(params)
    response[0].must_equal 200
  end
end
