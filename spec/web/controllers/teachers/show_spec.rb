require 'spec_helper'
require_relative '../../../../apps/web/controllers/teachers/show'

describe Web::Controllers::Teachers::Show do
  let(:action) { Web::Controllers::Teachers::Show.new }
  let(:params) { Hash[] }

  it 'is successful' do
    response = action.call(params)
    response[0].must_equal 200
  end
end
