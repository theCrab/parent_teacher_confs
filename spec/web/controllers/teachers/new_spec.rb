require 'spec_helper'
require_relative '../../../../apps/web/controllers/teachers/new'

describe Web::Controllers::Teachers::New do
  let(:action) { Web::Controllers::Teachers::New.new }
  let(:params) { Hash[] }

  it 'is successful' do
    response = action.call(params)
    response[0].must_equal 200
  end
end
