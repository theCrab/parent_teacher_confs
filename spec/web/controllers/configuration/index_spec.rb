require 'spec_helper'
require_relative '../../../../apps/web/controllers/configuration/index'

describe Web::Controllers::Configuration::Index do
  let(:action) { Web::Controllers::Configuration::Index.new }
  let(:params) { Hash[] }

  it 'is successful' do
    response = action.call(params)
    response[0].must_equal 200
  end
end
