require 'spec_helper'
require_relative '../../../../apps/web/controllers/parents/edit'

describe Web::Controllers::Parents::Edit do
  let(:action) { Web::Controllers::Parents::Edit.new }
  let(:params) { Hash[] }

  it 'is successful' do
    response = action.call(params)
    response[0].must_equal 200
  end
end
