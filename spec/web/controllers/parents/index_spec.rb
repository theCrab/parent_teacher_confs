require 'spec_helper'
require_relative '../../../../apps/web/controllers/parents/index'

describe Web::Controllers::Parents::Index do
  let(:action) { Web::Controllers::Parents::Index.new }
  let(:params) { Hash[] }

  it 'is successful' do
    response = action.call(params)
    response[0].must_equal 200
  end
end
