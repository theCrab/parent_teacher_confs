require 'spec_helper'
require_relative '../../../../apps/web/controllers/parents/show'

describe Web::Controllers::Parents::Show do
  let(:action) { Web::Controllers::Parents::Show.new }
  let(:params) { Hash[] }

  it 'is successful' do
    response = action.call(params)
    response[0].must_equal 200
  end
end
