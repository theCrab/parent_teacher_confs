require 'spec_helper'
require_relative '../../../../apps/web/controllers/parents/new'

describe Web::Controllers::Parents::New do
  let(:action) { Web::Controllers::Parents::New.new }
  let(:params) { Hash[] }

  it 'is successful' do
    response = action.call(params)
    response[0].must_equal 200
  end
end
