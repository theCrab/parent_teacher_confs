require 'spec_helper'
require_relative '../../../../apps/web/controllers/instructions/index'

describe Web::Controllers::Instructions::Index do
  let(:action) { Web::Controllers::Instructions::Index.new }
  let(:params) { Hash[] }

  it 'is successful' do
    response = action.call(params)
    response[0].must_equal 200
  end
end
