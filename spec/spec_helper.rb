# Require this file for unit tests
ENV['HANAMI_ENV'] ||= 'test'

require_relative '../config/environment'
require_relative './factories/factories'
require_relative './factories/stuff'

require 'minitest/autorun'
require 'factory_girl'

class Minitest::Unit::TestCase
  include FactoryGirl::Syntax::Methods
end

Hanami::Application.preload!
