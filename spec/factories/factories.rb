FactoryGirl.define do
  factory :parent do
    name 'Zigy'
    surname 'Biggy'
    email '345333wer'
    password "123"
    password_confirmation "123"

    to_create { |instance| ParentRepository.create(instance) }
  end
  
  factory :teacher do
    name 'Don'
    surname 'Corleone'
    email 'don@boss.com'
    password "111"
    role 'administrator'
    
    to_create { |instance| TeacherRepository.create(instance) }
  end
  
  
end