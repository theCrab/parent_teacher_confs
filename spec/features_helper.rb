# Require this file for feature tests
require_relative './spec_helper'
require_relative './factories/factories'
require_relative './factories/stuff'

require 'capybara'
require 'capybara/dsl'
require 'factory_girl'

Capybara.app = Hanami::Container.new

class MiniTest::Spec
  include Capybara::DSL
  include FactoryGirl::Syntax::Methods
end
